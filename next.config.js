module.exports = {
    webpack: config => {
        config.watchOptions = {
            poll: 1000,
            aggregateTimeout: 300,
        }
        return config
    },

    images: {
        domains: [
            "cdn.deepsense.space",
        ]
    },
    reactStrictMode: true,
    swcMinify: true,
    output: 'standalone',
}
