import Image from "next/image";
import React, {useState} from "react";
import Link from "next/link";


const ServerSideRendering = (props) => {
    const {sampleData, showServerSideData, setShowServerSideData, showBtn} = props
    const [showText, setShowText] = useState(false)

    const ShowServerSideRendering = () => {
        setShowServerSideData(!showServerSideData)
        setShowText(!showText)
    }


    return (<>
            {sampleData?.length > 0 && <div>
                <h1 className={"text-center"}>Server Side Rendering Data</h1>
                {
                    !showBtn &&
                    <Link style={{display: "flex", justifyContent: "center", paddingBottom: "30px"}}
                          href={"/client-side-rendering"}>Click here to
                        go client side
                        rendering</Link>}

                {
                    showBtn &&
                    <div className={'render-cta'}>
                        <p
                           onClick={() => ShowServerSideRendering()} className={'render-btn'}>Server
                            Side
                        </p>
                        {showText &&
                            <p className={'click-me'}>click me</p>
                        }
                    </div>
                }

                {
                    showServerSideData && <div className={'d-flex'}>
                        {sampleData?.map((data, index) => (<div key={index}>
                            <div>
                                <h4>{data.title}</h4>
                                <div>
                                    <Image src={data?.featuredImageUrl} alt={'alt'} width={370} height={250}/>
                                </div>
                                <h3>{data.description}</h3>
                            </div>
                        </div>))

                        }
                    </div>
                }


            </div>}
        </>

    )
}

export default ServerSideRendering