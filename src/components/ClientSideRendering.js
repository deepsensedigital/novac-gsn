import Image from "next/image";
import React, {useState} from "react";
import {gql, useQuery} from "@apollo/client";
import dynamic from "next/dynamic";
import Link from "next/link";


const ClientSideRenderings = (props) => {
    const {carouselType, showBtn, showClientSideData, setShowClientSideData} = props

    // const [showClientSideData,] = useState(false)
    const [showText, setShowText] = useState(false)

    const GET_DATA = gql`
        query getCarousel {
            getCarousel(carouselEnum:${carouselType}) {
                id
                description
                featuredImageUrl
                title
            }
        }
    `;

    const {loading, error, data} = useQuery(GET_DATA);

    if (loading) return <p className={'loader'}>Loading...</p>;
    if (error) return <p>Error : {error.message}</p>;

    const ShowClientSideDataFunc = () => {
        setShowClientSideData(!showClientSideData)
        setShowText(!showText)
    }


    return (<>
            {data?.getCarousel?.length > 0 &&
                <div>
                    <h1 className={"text-center"}>Client Side Rendering Data</h1>
                    {
                        showBtn &&
                        <div>
                            <p onMouseOver={() => setShowText(!showText)}
                               onClick={() => ShowClientSideDataFunc()}
                               className={'render-btn'}>Client
                                Side
                            </p>
                            {showText &&
                                <p className={'click-me-client-side'}>click me</p>
                            }
                        </div>
                    }
                    {
                        !showBtn &&

                        <Link style={{display: "flex", justifyContent: "center", paddingBottom: "30px"}}
                              href={"/cas-rendering"}>Click here to
                            go client side and server side
                            rendering</Link>
                    }
                    {
                        showClientSideData &&
                        <div className={'d-flex'}>
                            {data?.getCarousel?.map((data, index) => (<div key={index}>
                                <div>
                                    <h4>{data.title}</h4>
                                    <div>
                                        <Image priority={true}
	                                        src={data?.featuredImageUrl} alt={'alt'} width={370} height={250}/>
                                    </div>
                                    <h3>{data.description}</h3>
                                </div>
                            </div>))

                            }
                        </div>
                    }

                </div>
            }
        </>

    )
}

export default dynamic(() => Promise.resolve(ClientSideRenderings), {ssr: false})