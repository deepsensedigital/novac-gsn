import client from "../../Apollo/apollo-client";
import {gql} from "@apollo/client";
import ServerSideRendering from "@/components/ServerSideRendering";
import Link from "next/link";

const MainPage = (props) => {

	return (
		<div className={'text-center'}>
			<Link className={'p-3 flex-1'} href={'/server-side-rendering'}><h1 >ServerSide Rendering</h1></Link>
			<Link className={'p-3 flex-1'} href={'/client-side-rendering'}><h1 className={'p-3 flex-1'}>ClientSide Rendering</h1></Link>
			<Link className={'p-3 flex-1'} href={'/cas-rendering'}><h1 className={'p-3 flex-1'}>Comparison Rendering</h1></Link>
		</div>
	)
}

export default MainPage


