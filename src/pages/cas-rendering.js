import client from "../../Apollo/apollo-client";
import {gql} from "@apollo/client";
import ServerSideRendering from "@/components/ServerSideRendering";
import dynamic from "next/dynamic";
import React, {useState} from "react";
import ClientSideRendering from "@/components/ClientSideRendering";


const CheckCAS = (props) => {
    const {homepageQuery} = props
    const [showClientSideData, setShowClientSideData] = useState(true)
    const [showServerSideData, setShowServerSideData] = useState(true)


    return (
        <>
            <div className={'cas-rendering'}>

                <ClientSideRendering
                    setShowClientSideData={setShowClientSideData}
                    showClientSideData={showClientSideData}
                    showBtn={true}
                    carouselType={"SERVICES_CAROUSEL"}/>


                <ServerSideRendering
                    showBtn={true}
                    showServerSideData={showServerSideData}
                    setShowServerSideData={setShowServerSideData}
                    sampleData={homepageQuery?.HomeBanner}/>
            </div>
        </>

    )
}


export async function getServerSideProps({req, res}) {
    res.setHeader(
        'Cache-Control',
        'public, s-manage=10, stale-while-revalidate=59'
    )

    const {data} = await client.query({
        query: gql`
            query HomePage {
                HomeBanner:getCarousel(carouselEnum:SERVICES_CAROUSEL){
                    id
                    description
                    featuredImageUrl
                    title
                }
            }
        `,
    });

    return {
        props: {
            homepageQuery: data
        },
    };
}

export default dynamic(() => Promise.resolve(CheckCAS), {ssr: false})


