import React from "react";
import dynamic from "next/dynamic";
import ClientSideRenderings from "@/components/ClientSideRendering";


const ClientSideRendering = () => {


    return (<div>
            <ClientSideRenderings showClientSideData={true} showBtn={false} carouselType={"BANNER_CAROUSEL"}/>
        </div>

    )
}

export default dynamic(() => Promise.resolve(ClientSideRendering), {ssr: false})