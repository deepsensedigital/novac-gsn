import client from "../../Apollo/apollo-client";
import {gql} from "@apollo/client";
import ServerSideRendering from "@/components/ServerSideRendering";

const Home = (props) => {
	const {homepageQuery} = props
	console.log("homepageQuery", homepageQuery)

	return (
		<div>
			<ServerSideRendering showServerSideData={true} sampleData={homepageQuery?.HomeBanner}/>
		</div>
	)
}

export async function getServerSideProps({ res}) {
	res.setHeader(
		'Cache-Control',
		'public, s-manage=10, stale-while-revalidate=59'
	)

	const {data} = await client.query({
		query: gql`
            query HomePage {
                HomeBanner:getCarousel(carouselEnum:BANNER_CAROUSEL){
                    id
                    description
                    featuredImageUrl
                    title
                }
            }
        `,
	});

	return {
		props: {
			homepageQuery: data
		},
	};
}

export default Home


